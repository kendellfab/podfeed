package main

import (
	"log"

	"gitlab.com/kendellfab/podfeed/internal/resources"

	"fyne.io/fyne"
	"fyne.io/fyne/app"
	"fyne.io/fyne/dialog"
	"fyne.io/fyne/layout"
	"fyne.io/fyne/theme"
	"fyne.io/fyne/widget"
	"gitlab.com/kendellfab/podfeed/internal/extract"
	"gitlab.com/kendellfab/podfeed/internal/feed"
)

const (
	lastID   = "last_id"
	lastFeed = "last_feed"
)

func main() {
	app := app.NewWithID("us.kendellfabrici.podfeed")
	app.Settings().SetTheme(theme.LightTheme())
	app.SetIcon(resources.ActivityFeed)
	w := app.NewWindow("PodFeed")

	feedURL := widget.NewEntry()
	idEntry := widget.NewEntry()
	idEntry.SetPlaceHolder("iTunes podcast url")
	goForIt := widget.NewButton("Load Feed URL", func() {
		id, err := extract.ExtractID(idEntry.Text)
		if err != nil {
			dialog.ShowError(err, w)
			return
		}

		response, err := feed.LoadResponse(id)
		if err != nil {
			dialog.ShowError(err, w)
			return
		}
		log.Println(response)
		resFeed := response.Results[0].FeedURL
		feedURL.SetText(resFeed)
		app.Preferences().SetString(lastID, idEntry.Text)
		app.Preferences().SetString(lastFeed, resFeed)
	})

	link := "https://icons8.com/icons/set/activity-feed"
	linkLabel := widget.NewLabel(link)

	id := app.Preferences().String(lastID)
	if id != "" {
		idEntry.SetText(id)
	}

	feed := app.Preferences().String(lastFeed)
	if feed != "" {
		feedURL.SetText(feed)
	}

	//entryBox := widget.NewHBox(idEntry, goForIt)
	//entryBox.

	w.Resize(fyne.NewSize(400, 200))
	w.SetContent(fyne.NewContainerWithLayout(layout.NewVBoxLayout(), idEntry, goForIt, feedURL, layout.NewSpacer(), widget.NewHBox(layout.NewSpacer(), linkLabel, layout.NewSpacer())))

	w.ShowAndRun()
}
