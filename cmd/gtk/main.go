package main

import (
	"log"
	"os"

	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/gtk"
)

func main() {
	app, err := gtk.ApplicationNew("us.kendellfabrici.podfeed", glib.APPLICATION_FLAGS_NONE)
	if err != nil {
		log.Fatal("error starting application", err)
	}

	app.Connect("activate", func() {
		builder, err := gtk.BuilderNewFromFile("ui/podfeed.glade")
		if err != nil {
			log.Fatal("error opening builder", err)
		}

		obj, err := builder.GetObject("entryURL")
		if err != nil {
			log.Fatal("error loading entry url", err)
		}
		entryURL, ok := obj.(*gtk.Entry)
		if !ok {
			log.Fatal("could not parse entry url")
		}

		signals := map[string]interface{}{
			"on_btLoad_clicked": func(btLoad *gtk.Button) {
				log.Println(entryURL.GetText())
			},
		}
		builder.ConnectSignals(signals)

		obj, err = builder.GetObject("mainWindow")
		if err != nil {
			log.Fatal("could not load main window", err)
		}

		win, ok := obj.(*gtk.Window)
		if !ok {
			log.Fatal("not a window")
		}

		win.SetTitle("PodFeed")
		win.Show()
		app.AddWindow(win)
	})

	os.Exit(app.Run(os.Args))
}
