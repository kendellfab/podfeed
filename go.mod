module gitlab.com/kendellfab/podfeed

go 1.13

require (
	fyne.io/fyne v1.2.2
	github.com/gotk3/gotk3 v0.4.0
	golang.org/x/net v0.0.0-20200202094626-16171245cfb2 // indirect
)
