package feed

import (
	"encoding/json"
	"net/http"
)

func LoadResponse(id string) (ItunesResponse, error) {
	url := "https://itunes.apple.com/lookup"

	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return ItunesResponse{}, err
	}
	query := req.URL.Query()
	query.Add("id", id)

	req.URL.RawQuery = query.Encode()

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return ItunesResponse{}, err
	}
	defer resp.Body.Close()

	var itunesReponse ItunesResponse
	err = json.NewDecoder(resp.Body).Decode(&itunesReponse)
	return itunesReponse, err
}
