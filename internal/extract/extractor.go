package extract

import (
	"regexp"
	"strings"
)

// ExtractID takes an iTunes podcast url and extracts the ID.
// Example: https://podcasts.apple.com/us/podcast/side-hustle-school/id1188487073
func ExtractID(input string) (string, error) {

	idRegex, err := regexp.Compile(`id[0-9]+`)
	if err != nil {
		return "", err
	}

	res := idRegex.FindString(input)

	return strings.TrimPrefix(res, "id"), nil
}
