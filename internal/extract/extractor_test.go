package extract

import "testing"

func TestExtractor(t *testing.T) {

	id, err := ExtractID("https://podcasts.apple.com/us/podcast/side-hustle-school/id1188487073")
	if err != nil {
		t.Fatal(err)
	}

	expected := "1188487073"
	if id != expected {
		t.Errorf("error extracting id expected %s but got %s", expected, id)
	}
}
